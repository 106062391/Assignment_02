var Hexagram;
var loadState = {
    preload: function () {
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width / 2, 150,
            'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar
        var progressBar = game.add.sprite(game.width / 2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        // Load all game assets
        //game.load.image('player', 'Mari.png');
        game.load.spritesheet('player', 'character1.png', 32, 48);
        game.load.spritesheet('player2', 'reimuPlayer.png', 32, 48);
        game.load.spritesheet('coin_dead', 'coin_dead.png', 16, 16, 12);
        game.load.image('bullet', 'bullet.png');
        //game.load.image('enemy', 'bullet.png');
        game.load.image('wallH', 'ground.png');
        game.load.image('enemybullet', 'enemybullet.png');
        game.load.image('wallV', 'wallV.png');
        game.load.image('coin', 'coin.png');
        // Load a new asset that we will use in the menu state
        game.load.image('background', 'background.jpg');
        game.load.image('game_background', 'stage1.png');
        game.load.image('stage2', 'stage2.png');
        //game.load.image('pause', 'cirno.jpg');
        game.load.image('gameover', 'gameover.jpg');
        game.load.image('heart', 'heart.png');
        game.load.image('level1', 'level1.png');
        game.load.image('level2', 'infinite run.png');
        game.load.image('Makankosappo', 'Makankosappo.png');
        game.load.image('pixel', 'pixel.png');
        game.load.image('hexagram', 'hexagram.png');
        game.load.image('mushroom', 'mushroom.png');
        game.load.image('spellcard', 'spellcard.png');
        game.load.spritesheet('explode', 'explode.png', 43, 56);
        game.load.spritesheet('enemy', 'enemy.png', 33, 35);
        game.load.spritesheet('boss', 'sakuya.png', 32, 43);
        game.load.spritesheet('knife', 'sakuyaKnife.png', 32, 32);
        game.load.audio('bullet_sound', 'Laser Gun Sound Effect.mp3');
        game.load.audio('game_sound', 'Lunatic Princess.mp3');
        game.load.audio('menu_bgm', 'menu bgm.mp3');
    },
    create: function () {
        // Go to the menu state
        game.state.start('menu');
    }
}; 