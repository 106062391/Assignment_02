var left_walk;
var right_walk;
var no_walk;
var gameimage;
var enemy;
var bullets;
var weapon;
var fireButton;
var lazer;
var game_sound;
var keyP;
var keyO;
var GG;
var death = false;
var pause = false;
var lives;
var heart1;
var heart2;
var heart3;
var lifepoint = 3;
var ultimate_count = 0;
var ultimate_text;
var ultimate_text_count = 0;
var use_ult = false;
var enemyBullet;
var enemyBulletTime = 0; 
var livingEnemies = [];
var pause_text;
var continue_text;
var volume_text;
var velocityX;
var emitter2;
var mushroom_count = 0;
var mushroom_text;

var CoinParticle = (function () {
    var CoinParticle = function (game,x,y) {
        Phaser.Particle.call(this,game,x,y,'coin_dead');
        this.animations.add("rotate");
    };
    CoinParticle.prototype = Object.create(Phaser.Particle.prototype);
    CoinParticle.prototype.constructor = CoinParticle;
    CoinParticle.prototype.onEmit = function () {
        this.animations.stop("rotate",true);
        this.animations.play("rotate",60,true);
        this.animations.getAnimation('rotate').frame = Math.floor(Math.random() * this.animations.getAnimation('rotate').frameTotal);
    }
    return CoinParticle;
}());

var play2State = { 
    preload: function () { },
    create: function () {
        death = false;
        game.physics.startSystem(Phaser.Physics.ARCADE);

        gameimage = game.add.sprite(0, -1485, 'stage2');
        gameimage.width = game.width;
        gameimage.height = game.height*4;   
        game.physics.enable(gameimage, Phaser.Physics.ARCADE);

        ultimate_count = 0;
        mushroom_count = 0;
        use_ult = false;

        //player
        game.global.score = 0;
        this.cursor = game.input.keyboard.createCursorKeys();
        this.player = game.add.sprite(650, 350, 'player');
        this.player.frame = 23;
        this.player.animations.add('no_walk', [0, 1, 2, 3, 4, 5, 6, 7], 8, true);
        this.player.animations.add('left_walk', [8, 9, 10, 11, 12, 13, 14, 15], 8, true);
        this.player.animations.add('right_walk', [16, 17, 18, 19, 20, 21, 22, 23], 8, true);
        game.physics.arcade.enable(this.player);

        //  Lives
        lives = game.add.group();

        for (var i = 0; i < 3; i++) 
        {
            var heart1 = lives.create(1080 + (40 * i), 0, 'heart');
            heart1.scale.setTo(0.1, 0.1);
            //heart1.anchor.setTo(0.1, 0.1);
        }

        //keys
        keyP = game.input.keyboard.addKey(Phaser.Keyboard.P);
        keyO = game.input.keyboard.addKey(Phaser.Keyboard.O);
        var keyM = game.input.keyboard.addKey(Phaser.Keyboard.M);
        var keyX = game.input.keyboard.addKey(Phaser.Keyboard.X);
        var keyplus = game.input.keyboard.addKey(Phaser.Keyboard.TWO);
        var keyminus = game.input.keyboard.addKey(Phaser.Keyboard.ONE);
        var mute = false;

        keyP.onDown.add(function(){
            if(pause == false) pause_text = game.add.text(425, 190, 'Pause?', { font: '100px Segoe script', fill: 'white', backgroundColor: 'blue'});
            if(pause == false) continue_text = game.add.text(550, 400, 'Press O to Continue\n press 1 to decrease volume, 2 to increase', { font: '40 px Segoe script', fill: 'white', backgroundColor: 'blue'});
            if(pause == false) volume_text = game.add.text(1085, 50, 'Volume:' + game_sound.volume, { font: '20px Segoe script', fill: 'white', backgroundColor: 'blue'});
            game_sound.pause();
            pause = true;
        });
        keyO.onDown.add(function(){
            if(pause == true) pause_text.destroy();
            if(pause == true) continue_text.destroy();
            if(pause == true) volume_text.destroy();
            if(pause == true) game_sound.resume();
            pause = false;
        });
        keyM.onDown.add(function(){
            if(mute == false){
                if(pause == true){
                    volume_text.destroy();
                    volume_text = game.add.text(1085, 50, 'Volume:' + game_sound.volume, { font: '20px Segoe script', fill: 'white', backgroundColor: 'blue'});
                }
                game_sound.mute = true;
                mute = true;
            }
            else{
                if(pause == true){
                    volume_text.destroy();
                    volume_text = game.add.text(1085, 50, 'Volume:' + game_sound.volume, { font: '20px Segoe script', fill: 'white', backgroundColor: 'blue'});
                }
                game_sound.mute = false;
                mute = false;
            }
            
        });
        keyX.onDown.add(function(){
            if(ultimate_count == 5){
                //Makankosappo
                use_ult = true;
                game.time.events.add(Phaser.Timer.SECOND * 5, function(){
                    use_ult = false;
                });

                ultimate_text.destroy();
                ultimate_count = 0;
            }
        });
        keyplus.onDown.add(function(){
            game_sound.volume += 0.1;
            if(game_sound.volume>1) game_sound.volume = 1;

            if(pause == true){
                volume_text.destroy();
                volume_text = game.add.text(1085, 50, 'Volume:' + game_sound.volume, { font: '20px Segoe script', fill: 'white', backgroundColor: 'blue'});
            }
        });
        keyminus.onDown.add(function(){
            game_sound.volume -= 0.1;
            if(game_sound.volume<0.09) game_sound.volume = 0;

            if(pause == true){
                volume_text.destroy();
                volume_text = game.add.text(1085, 50, 'Volume:' + game_sound.volume, { font: '20px Segoe script', fill: 'white', backgroundColor: 'blue'});
            }
        });

        //extra item
        this.coin = game.add.sprite(60, 140, 'spellcard');
        this.coin.width = this.player.width+30;
        this.coin.height = this.player.height;
        game.physics.arcade.enable(this.coin);
        this.coin.anchor.setTo(0.5, 0.5);
        this.scoreLabel = game.add.text(30, 30, 'score: 0',
            { font: '18px Segoe script', fill: '#ffffff' });
        mushroom_text = game.add.text(950, 400, 'spellcard count: 0', { font: '25px Segoe script', fill: 'white', backgroundColor: 'blue'});

        //enemy
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.createMultiple(10, 'enemy');
        this.enemies.physicsBodyType = Phaser.Physics.ARCADE;
        game.time.events.loop(2500, this.addEnemy, this);

        //enemy bullet
        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true; 
        this.enemyBullets.physicsBodyType = Phaser.Physics.ARCADE; 
        this.enemyBullets.createMultiple(30, 'enemybullet');
        this.enemyBullets.setAll('scale.x', 0.2);
        this.enemyBullets.setAll('scale.y', -0.2);
        this.enemyBullets.setAll('anchor.x', 0);
        this.enemyBullets.setAll('anchor.y', 0);
        this.enemyBullets.setAll('outOfBoundsKill', true);
        this.enemyBullets.setAll('checkWorldBounds', true);

        //weapon&music
        lazer = game.add.audio('bullet_sound');
        game_sound = game.add.audio('game_sound');
        game_sound.loopFull();
        weapon = game.add.weapon(200, 'bullet');
        game.physics.arcade.enable(weapon);
        weapon.multiFire = true;
        weapon.bullets.setAll('scale.x', 0.2);
        weapon.bullets.setAll('scale.y', 0.2);
        //console.log(weapon.bullets.y);
        //  The bullet will be automatically killed when it leaves the world bounds
        weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
    
        //  Because our bullet is drawn facing up, we need to offset its rotation:
        weapon.bulletAngleOffset = 90;
    
        //  The speed at which the bullet is fired
        weapon.bulletSpeed = 400;

        weapon.trackSprite(this.player, 14, 0);
        fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);

        //particle
        //this.game.input.onTap.add(this.createParticle,this);
    },
    update: function () {
        if(death == false & pause == false){
            if(gameimage.y < -10) gameimage.body.velocity.y = 50;
            if(gameimage.y > -10){
                gameimage.y = -1485;
            }
        }
        if(pause == true) gameimage.body.velocity.y = 0;

        if(fireButton.isDown && death == false && use_ult == true && pause == false){
            //weapon.fire();
            if (fireButton.isDown)
            {
                weapon.fireRate = 0;
                weapon.fire({ x: this.player.x, y: this.player.y });
                weapon.fire({ x: this.player.x + 10, y: this.player.y });
                weapon.fire({ x: this.player.x + 20, y: this.player.y });
            }
            weapon.fireRate = 1000;
            lazer.play();
        }
        else if(fireButton.isDown && death == false && use_ult == false &&  pause == false){
            weapon.fireRate = 100;
            weapon.fire();
            lazer.play();
        }

        if (!this.player.inWorld) {
            death = true;
            this.playerDie();
        }

        //ultimate
        if(ultimate_count==0 && ultimate_text_count == 0 && death == false){
            ultimate_text = game.add.text(920, 450, 'Kill To Gain Energy!', { font: '25px Segoe script', fill: 'white', backgroundColor: 'blue'});
            ultimate_text_count = 1;
        }
        else if(ultimate_count==1 && ultimate_text_count == 0 && death == false){
            ultimate_text.destroy();
            ultimate_text = game.add.text(1050, 450, '魔', { font: '25px Segoe script', fill: 'white', backgroundColor: 'blue'});
            ultimate_text_count = 1;
        }
        else if(ultimate_count==2 && ultimate_text_count == 0 && death == false){
            ultimate_text.destroy();
            ultimate_text = game.add.text(1050, 450, '魔貫', { font: '25px Segoe script', fill: 'white', backgroundColor: 'blue'});
            ultimate_text_count = 1;
        }
        else if(ultimate_count==3 && ultimate_text_count == 0 && death == false){
            ultimate_text.destroy();
            ultimate_text = game.add.text(1050, 450, '魔貫光', { font: '25px Segoe script', fill: 'white', backgroundColor: 'blue'});
            ultimate_text_count = 1;
        }
        else if(ultimate_count==4 && ultimate_text_count == 0 && death == false){
            ultimate_text.destroy();
            ultimate_text = game.add.text(1050, 450, '魔貫光殺', { font: '25px Segoe script', fill: 'white', backgroundColor: 'blue'});
            ultimate_text_count = 1;
        }
        else if(ultimate_count==5 && ultimate_text_count == 0 && death == false){
            ultimate_text.destroy();
            ultimate_text = game.add.text(980, 450, '魔貫光殺砲!!', { font: '40px Segoe script', fill: 'red', backgroundColor: 'blue'});
            ultimate_text_count = 1;
        }

        game.physics.arcade.collide(this.player, this.floor);
        game.physics.arcade.collide(this.player, this.walls);
        game.physics.arcade.collide(this.enemies, this.walls);
        game.physics.arcade.collide(this.enemies, this.floor);
        game.physics.arcade.collide(this.enemies, weapon);
        game.physics.arcade.overlap(weapon.bullets, this.enemies, this.EnemyDie, null, this);
        game.physics.arcade.overlap(this.player, this.coin,this.takeCoin, null, this);
        //game.physics.arcade.overlap(this.player, this.enemies, this.playerDie, null, this);
        //game.physics.arcade.overlap(this.enemyBullets, this.player, this.playerDie, null, this);
        game.physics.arcade.overlap(this.player, this.enemies, this.enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(this.enemyBullets, this.player, this.createParticle, null, this);
        game.physics.arcade.overlap(this.enemyBullets, this.player, this.enemyHitsPlayer, null, this);
        


        livingEnemies.length = 0; 
        this.enemies.forEachAlive(function(enemy){
            livingEnemies.push(enemy)
        });

        if(this.time.now > enemyBulletTime && pause == false) { 
            this.enemyBullet = this.enemyBullets.getFirstExists(false); 
            if(this.enemyBullet && livingEnemies.length > 0) {
                
                var random = this.rnd.integerInRange(0, livingEnemies.length - 1);
                var shooter = livingEnemies[random];
                this.enemyBullet.reset(shooter.body.x, shooter.body.y + 70);
                enemyBulletTime = this.time.now + 500;
                this.enemyBullet.body.velocity.y=250;
                //if(pause == true) this.enemyBullet.body.velocity.y=0;
                //if(pause == false) this.enemyBullets.setAll('body.velocity.y', 250);
            }
        }

        if(pause == true) this.enemyBullets.setAll('body.velocity.y', 0);
        if(pause == false) this.enemyBullets.setAll('body.velocity.y', 250);

        if(pause == true){
            this.enemies.setAll('body.velocity.y', 0);
            this.enemies.setAll('body.velocity.x', 0);
        }
        else if(pause == false){
            this.enemies.setAll('body.velocity.y', 100);
            this.enemies.setAll('body.velocity.x', velocityX);
        }

        if(pause == true) weapon.bullets.setAll('body.velocity.y', 0);
        if(pause == false) weapon.bullets.setAll('body.velocity.y', -400);

        if(pause == false) this.movePlayer();
    }, // No changes
    movePlayer: function () {

        if (this.cursor.left.isDown){
            this.player.body.x -= 5;
            this.player.animations.play('left_walk');
        }
        else if (this.cursor.right.isDown){
            this.player.body.x += 5; 
            this.player.animations.play('right_walk');
        }
        else{
            this.player.animations.play('no_walk');
        }
        if (this.cursor.up.isDown)
            this.player.body.y -= 5; 
        if (this.cursor.down.isDown)
            this.player.body.y += 5;

    }, // No changes 
    takeCoin: function (player, coin) {
        // Use the new score variable game.global.score += 5;
        // Use the new score variable
        game.global.score += 5;
        this.scoreLabel.text = 'score: ' + game.global.score;

        mushroom_count++;
        mushroom_text.destroy();
        if(mushroom_count % 10 == 0){
            weapon.bullets.setAll('scale.x', 0.5);
            weapon.bullets.setAll('scale.y', 0.5);
            game.time.events.add(Phaser.Timer.SECOND * 5, function(){
                weapon.bullets.setAll('scale.x', 0.2);
                weapon.bullets.setAll('scale.y', 0.2);
            });
        }

        mushroom_text = game.add.text(950, 400, 'spellcard count: '+mushroom_count, { font: '25px Segoe script', fill: 'white', backgroundColor: 'blue'});
        

        // Then no changes
        var coinPosition = [{ x: 140, y: 60 }, { x: 360, y: 60 },
        { x: 60, y: 140 }, { x: 440, y: 140 },
        { x: 130, y: 300 }, { x: 370, y: 300 },{ x: 1000, y: 300}, { x: 1100, y: 60}, { x: 800, y: 140}, { x: 900, y: 100 }, { x: 500, y: 200}, { x: 200, y: 250 } ];
        for (var i = 0; i < coinPosition.length; i++) {
            if (coinPosition[i].x == this.coin.x) {
                coinPosition.splice(i, 1);
            }
        }
        var newPosition = game.rnd.pick(coinPosition);
        coin.reset(newPosition.x, newPosition.y);
    },
    createParticle:function () {
        //console.log('came in');
        var emitter = this.game.add.emitter(this.player.x,this.player.y,0);
        emitter.width = 10;
        emitter.setXSpeed(-100,100);
        emitter.setYSpeed(-100,100);
        emitter.particleClass = CoinParticle;
        emitter.makeParticles();
        emitter.gravity = 200;
        emitter.start(true,3000,null,50);
    },
    //updateCoinPosition: function() { }, // No changes
    addEnemy: function () {
        var enemy = this.enemies.getFirstDead();
        if (!enemy) { return; }
        enemy.anchor.setTo(0.5, 1);
        //enemy.reset(game.width / 2, 0);
        var random_position = game.rnd.integerInRange(game.width*1/4, game.width*3/4);
        if(pause == false){
            enemy.reset(random_position, 0);
            enemy.body.velocity.y = 100;
            enemy.body.velocity.x = 100 * game.rnd.pick([-2,-1,1, 2]);
            enemy.body.bounce.x = 1;
            enemy.checkWorldBounds = true;
            enemy.outOfBoundsKill = true;
        }

        velocityX = enemy.body.velocity.x;
    }, // No changes
    enemyHitsPlayer: function (player,bullet) {
    
        bullet.kill();
    
        live = lives.getFirstAlive();
    
        if (live)
        {
            live.kill();
        }
    
        // When the player dies
        if (lives.countLiving() < 1)
        {
            death = true;
            this.enemyBullets.callAll('kill');

            keyD = game.input.keyboard.addKey(Phaser.Keyboard.D);
            var keyF = game.input.keyboard.addKey(Phaser.Keyboard.F);
            game_sound.stop();
            gameimage = game.add.sprite(0, 0, 'gameover');
            gameimage.width = game.width;
            gameimage.height = game.height;
            var text = game.add.text(game.world.centerX, game.world.centerY+150, 'Press D to continue, or Press F to start again');
            text.addColor('white', 0);
            text.anchor.set(0.5);
            text.align = 'center';
            keyD.onDown.add(function(){
                game.state.start('rank');
            });
            keyF.onDown.add(function(){
                game.state.start('play');
                death = false;
            });
        }
    
    },
    playerDie: function (enemyBullet,player) {

        death = true;
        ultimate_text.destroy(true);
        this.enemyBullets.callAll('kill');

        keyD = game.input.keyboard.addKey(Phaser.Keyboard.D);
        var keyF = game.input.keyboard.addKey(Phaser.Keyboard.F);
        game_sound.stop();
        gameimage = game.add.sprite(0, 0, 'gameover');
        gameimage.width = game.width;
        gameimage.height = game.height;
        var text = game.add.text(game.world.centerX, game.world.centerY+150, 'Press D to continue, or Press F to start again');
        text.addColor('white', 0);
        text.anchor.set(0.5);
        text.align = 'center';
        keyD.onDown.add(function(){
            game.state.start('rank');
        });
        keyF.onDown.add(function(){
            game.state.start('play');
            death = false;
        });
    },
    EnemyDie: function(bullet,enemy){
        game.global.score += 5;
        this.scoreLabel.text = 'score: ' + game.global.score;

        emitter2 = game.add.emitter(enemy.x, enemy.y, 0);
        emitter2.makeParticles('hexagram');
        emitter2.setScale(0.1, 0.1, 0.1, 0.1);
        emitter2.start(true, 800, null, 15);

        if(ultimate_count<5) ultimate_count++;
        ultimate_text_count--;

        enemy.kill();
        //this.enemies.remove(enemy);
        bullet.kill();
    }
};
        // Delete all Phaser initialization code



        
        /*this.floor = game.add.sprite(400, game.height-50, 'wallH');
        this.floor.scale.setTo(1, 1);
        game.physics.arcade.enable(this.floor);
        this.floor.body.immovable = true;

        this.walls = game.add.group();
        this.walls.enableBody = true;
        game.add.sprite(0, 0, 'wallV', 0, this.walls);        
        this.walls.setAll('body.immovable', true);*/

        /*
1.pause
-怪物要暫停(v)
-音量顯示(v)

2.boss&怪物生成範圍變大(v)

3.stage2 (v)

4.角色2 && 講解

5.必殺:子彈快到變激光炮 (v)

6.吃道具(v)
-子彈變大(v)

7.firebase (v)
-player name (v)

8.(不是必要)字體美觀 (v)

9. & enemy particle!!(v)
*/