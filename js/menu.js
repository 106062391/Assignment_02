var bgm;
var game_start = false;

var menuState = {
    create: function () {
        // Add a background image
        game.add.image(0, 0, 'background');

        if(!game_start){
            bgm = game.add.audio('menu_bgm');
            game_start = true;
            bgm.loopFull();
        }
        // Display the name of the game
        var nameLabel = game.add.text(game.width / 2, 80, ' Not Touhou Project',
            { font: '50px Segoe script', fill: '#ffffff', backgroundColor:'blue'});
        nameLabel.anchor.setTo(0.5 ,0.5);
        // Explain how to start the game
        var startLabel = game.add.text(game.width / 2, game.height - 80,
            'press UP to start', { font: '25px Segoe script', fill: '#ffffff' });
        startLabel.anchor.setTo(0.5, 0.5);
        // Create a new Phaser keyboard variable: the up arrow key
        // When pressed, call the 'start'
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        upKey.onDown.add(this.start, this);
    },
    start: function () {
        // Start the actual game
        //bgm.stop();
        game.state.start('level');
    }
}; 

var level1;
var level2;

var levelState = {
    create: function () {
        var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        var keyOne = game.input.keyboard.addKey(Phaser.Keyboard.ONE);
        var keyTwo = game.input.keyboard.addKey(Phaser.Keyboard.TWO);
        downKey.onDown.add(function(){
            game_start = true;
            game.state.start('menu');
        })

        game.add.image(0, 0, 'background');
        
        level1 = game.add.image(80, 50, 'level1');   
        level1.height = game.height / 4;
        level1.width = game.width / 4;
        game.add.text(170, 190, 'Boss Run', { font: '25px Segoe script', fill: 'white', backgroundColor: 'blue'});
        game.add.text(170, 215, 'Press 1', { font: '10px Segoe script', fill: 'white'});
        keyOne.onDown.add(function(){
            bgm.stop();
            game_start = false;
            game.state.start('play');
        });

        level2 = game.add.image(450, 50, 'level2');   
        level2.height = game.height / 4;
        level2.width = game.width / 4;
        game.add.text(520, 190, 'Infinite Run', { font: '25px Segoe script', fill: 'white', backgroundColor: 'blue'});
        game.add.text(520, 215, 'Press 2', { font: '10px Segoe script', fill: 'white'});
        keyTwo.onDown.add(function(){
            bgm.stop();
            game_start = false;
            game.state.start('play2');
        });

        game.add.text(470, 400, 'Press DOWN to go back', { font: '25px Segoe script', fill: 'white', backgroundColor: 'blue'});
    }
}; 