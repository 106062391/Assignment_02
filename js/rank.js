var sprite;
var weapon;
var cursors;
var fireButton;
var astr_falling = true;
var name = true;
var rankState = {
    preload:function(){
        game.load.image('bg', 'cirno.jpg');
    },
    create:function(){
        var player = prompt("Please Enter Your Name:", "name");
        if(player == null) player = 'player without name';

        this.cursor = game.input.keyboard.createCursorKeys();

        //game.add.tileSprite(0, 0, 1205, 495, 'bg').scale.setTo(1,1);
        var image = game.add.image(0,0,'bg');
        image.width = game.width;
        image.height = game.height;
        //menu text
        var style = {fill: 'white', font: '20px Segoe script', backgroundColor: 'green'};
        var title_style = {fill: 'white', font: '40px Segoe script', backgroundColor: 'green'};
        this.NS = game.add.text(500, 40, 'Ranking', title_style);
        //game.add.text(500, 50, 'Leaderboard', {fill: 'white', fontSize: '50px', backgroundColor: 'green'});
        this.index = game.add.text(500, 360, 'Press space to go back to menu', style);
        
        
       
        
        if ( game.global.score) {
            var Ref = firebase.database().ref('score_list');
            var data = {
                name: player,
                score : Math.round(game.global.score)
            }
            Ref.push(data);
            var text1 = game.add.text(100, 300, 'Your Name: ' + player, style);
            var text2 = game.add.text(100, 350, 'Your Score: ' + Math.round(game.global.score), style);
        }
        
        
        //firebase output
        var postsRef = firebase.database().ref('score_list').orderByChild('score').limitToLast(5);

        var num = [];
        var user = [];
        var place = 1;
        var style2 = {fill: 'white', fontSize: '20px'}
        postsRef.once('value').then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                num.push(childSnapshot.val().score);
                user.push(childSnapshot.val().name);
            });
          }).then(function(style){
            for(var i=4; i>=0; i--){
                if(num[i])
                {
                    game.add.text(500, 250-i*30, 'N0.' + place.toString() + '    ' + user[i].toString() + '    ' + num[i].toString(), style).anchor.setTo(0, 0.5);
                    place++;
                }
            }
        });

        //this.gameoverSound = game.add.audio('gameOver');
        //this.gameoverSound.play();
        
    },
    update:function(){

      
        var keyboard3 = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        if(keyboard3.isDown){
            game.state.start('menu');
        }
    }

    
};