// Initialize Phaser
//ar game = new Phaser.Game(1000, 680, Phaser.AUTO, 'canvas');
var game = new Phaser.Game(1205, 495, Phaser.AUTO, 'canvas');
// Define our global variable
game.global = { score: 0 };
// Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('rank', rankState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('level', levelState);
game.state.add('play2', play2State);
// Start the 'boot' state
game.state.start('boot');
//game.state.start('boot');