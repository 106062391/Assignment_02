READ ME

Complete game process
-	Starts from the “menu” state, then goes to the level state for players to choose what level he/she wants to play. After playing the game, it goes to the “rank” state, which shows the leaderboard. At last, it goes back to the menu.
Basic rules
-	Inside the “play” & “play2” state, there’s a controllable player that can shoot, and lives would decrease if hit by an enemy bullet/enemy. 
-	A few enemies that randomly generates and can attack while moving.
-	A movable map, which works slightly differently in different modes.
Jucify mechanisms
-	2 Different levels for players to choose
-	There’s an ultimate skill that can be used by killing 5 separate enemies, but needs to be charged again after 5 seconds of using the ultimate skill.
Animations
-	2 different players with animations when they’re moving left, right, or just simply standing still.
Particle Systems
-	When an enemy/enemy bullets hits the player, there will be money splitting out of the player.
-	When the player’s bullet kills an enemy, there will be hexagrams splitting out of the enemy.
Sound effects
-	2 BGMs, one in menu, the other in the 2 levels
-	Sound effect for player shooting bullets.
UI
-	Player health on the right top corner
-	Ultimate charge up bar in the right bottom corner
-	Score in the left top corner
-	Volume shows when game pauses, which is when the player presses the keyboard P, and game would be resumed if the player presses keyboard O.
Leaderboard
-	After GameOver, player would be lead to the “rank” state, where they can enter their username, and the hall of fame would be shown to them, where the scores are collected in firebase, although if the player scored 0, they won’t get recorded. Too embarrassing.

Bonus
-	Enhanced item
o	There are spellcards in the game which can be taken. It grants points, and after collecting 10 of them, player shoot out unique bullets for 5 seconds.
-	Boss
o	First level contains a boss, where Player needs to either defeat the boss or it’s gameover, and the boss will shoot out illusion bullets.
